<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtRefreshMiddleware
{
        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            try {
                $refresh_token = JWTAuth::getToken();
                $token = auth('api')->refresh($refresh_token);
                JWTAuth::setToken($token);
            } catch (Exception $e) {
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException){
                    return response()->json(['status' => 'Token in black list',
                                             'code' => 5],401);
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                    return response()->json(['status' => 'Token is Expired',
                                             'code' => 6],401);
                } else {
                  return "";
                }
            }
            $request->request->add(['new_token' => $token]);
            $request->request->add(['refresh_token' => $refresh_token]);
            return $next($request);
        }
}
