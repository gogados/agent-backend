<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;
use \App\Models\Buildings;
use \App\Models\Flat;
use \App\Models\Clients;
use \App\Models\Followers;
use \App\Search\FlatSearch;
use Illuminate\Support\Facades\DB;



class AgentController extends Controller
{
    public function getBuildings()
    {
      return Buildings::all();
    }

    public function buildingsName()
    {
      return Buildings::select('building_name')->get();
    }

    public function search(Request $request)
    {
      $query = Flat::
      join('layout', 'layout.id', '=', 'flat.layout_id')
      ->select('buildings.*',DB::raw("count(flat.number) as countFlat"),'users.description as about_builder')
      ->join('constructions', 'constructions.construction','=','layout.construction_id')
      ->join('buildings', 'buildings.building','=','constructions.building_id')
      ->join('users', 'users.id','=','buildings.builder_id')
      ->groupBy('buildings.building');

      return \App\Search\FlatSearch::apply($query,$request);

    }

    public function getBuilding(Request $request)
    {
      if(!$request->has('building_id'))
      {
        abort(400);
      }
      $query = Flat::
      join('layout', 'layout.id', '=', 'flat.layout_id')
      ->select('constructions.construction as construction_id',
      'constructions.name as construction_name',
      'constructions.year_completion as construction_year_completion',
      'constructions.quarter_completion as construction_quarter_completion',
      'constructions.completed as construction_completed',
      'layout.count_rooms as rooms',
      'layout.square as square',
      'layout.photo as photo',
      'flat.level as level',
      'flat.price as price',
      'flat.id as flat_id', 
      'flat.status_id as status_code',
      'flat_status.status_name as status_name',
      'flat.number as number')
      ->join('constructions', 'constructions.construction','=','layout.construction_id')
      ->join('buildings', 'buildings.building','=','constructions.building_id')
      ->join('flat_status', 'flat_status.id','=','flat.status_id')
      ->orderBy('layout.construction_id')
      ->where('flat.status_id', '<', '3');

      return \App\Search\FlatSearch::apply($query,$request);
    }

    public function flat(Request $request)
    {
      if(!$request->has('id'))
      {
        abort(400);
      }
      return Flat::where('id','=',$request['id'])->get();
    }

    public function subscribe(Request $request)
    {
      if(!$request->has('flat'))
      {
        return response()->json(['status' => 'Flat id was not found in request',
                                  'code' => 7],400);
      }

      $flat = Flat::find($request->get('flat'));
      if(!$flat)
      {
        return response()->json(['status' => 'Flat not found',
        'code' => 8],400);
      }
      $apy = JWTAuth::getPayload(JWTAuth::getToken())->toArray();
      $user = User::find($apy['sub']);
      if(Followers::where('rieltor_id','=',$user->id)
        ->where('flat_id','=',$flat->id)
        ->count() > 0)
        {
          return response()->json(['status' => 'You are already subscribed',
          'code' => 9],400);
        }
      $follow = new Followers;
      $follow->rieltor()->associate($user);
      $follow->flat()->associate($flat);
      $follow->save();
      return response()->json(['status' => 'success',
      'code' => 0],200);
    }

    public function unSubscribe(Request $request)
    {
      if(!$request->has('flat'))
      {
        return response()->json(['status' => 'Flat id was not found in request',
                                  'code' => 7],400);
      }

      $flat = Flat::find($request->get('flat'));
      if(!$flat)
      {
        return response()->json(['status' => 'Flat not found',
        'code' => 8],400);
      }
      $apy = JWTAuth::getPayload(JWTAuth::getToken())->toArray();
      $user = User::find($apy['sub']);
      $follows = Followers::where('rieltor_id','=',$user->id)
        ->where('flat_id','=',$flat->id)
        ->get();
      if(count($follows) < 1)
      {
        return response()->json(['status' => 'You didn\'t follow for this flat',
        'code' => 10],400);
      }
      foreach ($follows as $follow) {
        $follow->delete();
      }
      
      return response()->json(['status' => 'success',
      'code' => 0],200);
    }

    public function subscriptions()
    { 
      $apy = JWTAuth::getPayload(JWTAuth::getToken())->toArray();
      $user = User::find($apy['sub']);
      return Followers::select('followers.flat_id as flat_id', 
      'followers.has_changed as has_changed',
      'constructions.construction as construction_id',
      'constructions.name as construction_name',
      'constructions.completed as construction_completed',
      'buildings.building_name as building_name',
      'layout.count_rooms as rooms',
      'layout.square as square',
      'layout.photo as photo',
      'flat.level as level',
      'flat.price as price',
      'flat.id as flat_id', 
      'flat.status_id as status_code', 
      'flat_status.status_name as status_name',
      'flat.number as number')
      ->where('rieltor_id','=',$user->id)
      ->join('flat', 'flat.id', '=', 'followers.flat_id')
      ->join('layout', 'layout.id', '=', 'flat.layout_id')
      ->join('constructions', 'constructions.construction','=','layout.construction_id')
      ->join('buildings', 'buildings.building','=','constructions.building_id')
      ->join('flat_status', 'flat_status.id','=','flat.status_id')
      ->get();
    }

    public function clients ()
    {
        $apy = JWTAuth::getPayload(JWTAuth::getToken())->toArray();
        $user_id = $apy['sub'];
        return Clients::select('clients.*')->where('clients.rieltor_id',$user_id)->get();
    }

    public function createClient (Request $request)
    {
      $apy = JWTAuth::getPayload(JWTAuth::getToken())->toArray();
      $user = User::find($apy['sub']);
      $first_name = $request->get('first_name');
      $last_name = $request->get('last_name');
      $patronymic = $request->get('patronymic');
      $phone = $request->get('phone');

      if(!$phone || !$first_name) {
        return response()->json(['status' => 'Phone and first_name is required parametrs',
        'code' => 11],400);
      }
      
      $client = new Clients;

      $client->rieltor()->associate($user);
      $client->first_name = $first_name;
      $client->last_name = $last_name;
      $client->patronymic = $patronymic;
      $client->phone = $phone;

      $client->save();

      return response()->json(['status' => 'success',
      'code' => 0],200);
    }

    public function updateClient (Request $request, $id)
    {
      $apy = JWTAuth::getPayload(JWTAuth::getToken())->toArray();
      $user = User::find($apy['sub']);
      $first_name = $request->get('first_name');
      $last_name = $request->get('last_name');
      $patronymic = $request->get('patronymic');
      $phone = $request->get('phone');

      if(!$phone || !$first_name) {
        return response()->json(['status' => 'Phone and first_name is required parametrs',
        'code' => 11],400);
      }
      
      $client = Clients::find($id);

      if(!$phone || !$first_name) {
        return response()->json(['status' => 'Client not found',
        'code' => 12],400);
      }

      $client->first_name = $first_name;
      $client->last_name = $last_name;
      $client->patronymic = $patronymic;
      $client->phone = $phone;

      $client->save();

      return response()->json(['status' => 'success',
      'code' => 0],200);
    }

    public function removeClient ($id)
    {
      $apy = JWTAuth::getPayload(JWTAuth::getToken())->toArray();
      $user_id = $apy['sub'];
      $client = Clients::find($id);
      if(!$client)
      {
        return response()->json(['status' => 'Client not found.',
        'code' => 12],400);
      }
      if($client->rieltor_id != $user_id)
      {
        return response()->json(['status' => 'Isn\'t your client!',
        'code' => 13],403);
      }
      $client->delete();
      return response()->json(['status' => 'success',
      'code' => 0],200);
    }
}
