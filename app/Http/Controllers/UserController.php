<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;



class UserController extends Controller
{
    public function register(Request $request)
    {
      $start = microtime(true);
            $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'phone' => 'unique:rieltors|required|regex:/^[+]{0,1}[0-9]{1,4}/', 
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
                //Log::stack(['slack'])->warning($validator->errors().' for user phone: '.$request->get('phone'));
                return response()->json($validator->errors(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'password' => Hash::make($request->get('password')),
        ]);
      $token = JWTAuth::fromUser($user);
      $time = microtime(true) - $start;
        //Log::stack(['slack'])->notice('was sign up new user. Name: '.$user->name.'. Phone: '.$user->phone.'. Execution time: '.strval($time*1000).' msec');
        return response()->json(compact('user','token'),201);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('phone', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = JWTAuth::user();
        $user->refresh_token = JWTAuth::fromUser($user);
        $user->update();

        return $this->respondWithToken($token, $user->refresh_token);
    }

    public function logout(Request $request)
    {
      auth()->logout();
    }

    public function RefreshToken(Request $request)
    {
      try
      {
        $new_token = $request->get('new_token');
        $refresh_token = $request->get('refresh_token');

        $apy = JWTAuth::getPayload($new_token)->toArray();
        $user = User::find($apy['sub']);
        if($user->refresh_token != $refresh_token)
        {
          return response()->json(['status' => 'Not valid refresh token',
                                    'code' => 3],401);
        }
        $user->refresh_token = JWTAuth::fromUser($user);
        $user->update();
      }
      catch (JWTException $e)
      {
        return response()->json(['status' => 'Undefined error',
                                  'code' => 4],401);
      }

      return $this->respondWithToken($new_token, $user->refresh_token); 
    }

    public function getAuthenticatedUser()
    {
            try {

                    if (! $user = JWTAuth::parseToken()->authenticate()) {
                            return response()->json(['user_not_found'], 404);
                    }

            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                    return response()->json(['token_expired'], $e->getStatusCode());

            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                    return response()->json(['token_invalid'], $e->getStatusCode());

            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                    return response()->json(['token_absent'], $e->getStatusCode());

            }

            return response()->json(compact('user'));
    }

        /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $refreshToken)
    {
        return response()->json([
            'access_token' => $token,
            'refresh_token' => $refreshToken,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
