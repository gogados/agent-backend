<?php

namespace App\Search;

use App\User;
use Illuminate\Http\Request;

class FlatSearch
{
  public static function apply($query,Request $filters)
  {
    if($filters->has('name'))
    $query = $query->where('buildings.building_name', 'LIKE', $filters['name']);

    if($filters->has('price')) //формат млн.руб
    {
        $filter = $filters['price'];
        $range = $filter['rangeData']['array'];
        $query->where('flat.price', '>=', $range[0]*1000000);
        if($range[1] > 0 )
        {
          $query->where('flat.price', '<=', $range[1]*1000000);
        }
    }
    if($filters->has('square')) //формат млн.руб
    {
        $filter = $filters['square'];
        $range = $filter['rangeData']['array'];
        $query->where('layout.square', '>=', $range[0]);
        if($range[1] > 0 )
        {
          $query->where('layout.square', '<=', $range[1]);
        }
    }
    if($filters->has('endofbuild'))
    {
        $filter = $filters['endofbuild'];
        $range = $filter['arrayKey'];
        if($range[0] == 0)
          $query->where('constructions.completed', '=', '1');
        else
          $query->whereYear('constructions.year_completion', '<=', $range[0]);
    }
    if($filters->has('typeflat'))
    {
        $filter = $filters['typeflat'];
        $keys = $filter['arrayKey'];
        $query->whereIn('layout.count_rooms', $keys);
    }
    if($filters->has('count_room'))
    {
      $query->where('layout.count_rooms', '=', $filters['count_room']);
    }
    if($filters->has('levelb'))
    {
      $query->where('flat.level', '>=', $filters['levelb']);
    }
    if($filters->has('levele'))
    {
      $query->where('flat.level', '<=', $filters['levele']);
    }
    if($filters->has('building_id'))
    {
      $query->where('buildings.building', '=', $filters['building_id']);
    }
    return $query->get();

  }
}

?>