<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
       /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */ 
    protected $table = 'clients';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['rieltor_id','first_name','last_name','patronymic','phone'];

    public function rieltor()
    {
      return $this->belongsTo('App\User');
    }
}
