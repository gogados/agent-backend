<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $connection = 'mysql_admin';    
    protected $table = 'constructions';
    protected $primaryKey = 'construction';
    // public $timestamps = false;
    protected $guarded = ['construction'];
    protected $fillable = ['name','building_id','number','completed','year_completion','quarter_completion','count_lvl','count_flats','count_porch'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function building()
    {
      return $this->belongsTo('App\Models\Buildings','building_id')->with('user');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
