<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::post('logout', 'UserController@logout');

Route::group(['middleware' => ['api.refresh']], function() {
  Route::post('refresh-token', 'UserController@RefreshToken');
});
Route::group(['middleware' => ['jwt.verify']], function() {
  Route::get('user', 'UserController@getAuthenticatedUser');
  Route::get('buildings', 'AgentController@getBuildings');
  Route::get('buildings/names','AgentController@buildingsName');
  Route::post('search','AgentController@search');
  Route::post('building','AgentController@getBuilding');
  Route::post('flat','AgentController@flat');

  Route::post('subscribe','AgentController@subscribe');
  Route::post('unsubscribe','AgentController@unSubscribe');
  Route::get('subscriptions','AgentController@subscriptions');

  Route::get('clients','AgentController@clients');
  Route::post('clients/create','AgentController@createClient');
  Route::post('clients/update/{id}','AgentController@updateClient');
  Route::post('clients/remove/{id}','AgentController@removeClient');
});


